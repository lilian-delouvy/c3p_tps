(* Exercice 1 *)
(* 1+2 renvoie un type int *)
(* 2: int, 2.0: float, 2,0: int * int, 2;0: int avec warning, a: erreur, 'a': 
   char, "a": string, true:bool, ():unit, []: 'a list, [1]: int list, [1,true]:
   (int * bool) list, [1; true]: erreur *)
(* 1,2.0: int*float, ["bonjour"]:string list, [true],"bonjour": bool list *
   string *)

let max a b = 
  if a > b then a else b;;

let min a b c = if a < b then (if a < c then a else c);;

let isPair a =
  if (a mod 2) = 0 then string_of_int a else "odd";;

let corrected a =
  if a<10 then "small" else "large";;

let averageInt a b c =
  (a+b+c)/3;;

let averageFloat a b c =
  (a+.b+.c)/.3.0;;

let implies a b =
  if a && not b then false
  else true ;;

(* fst : 'a * 'b -> 'a = <fun> ; snd est de même type. fst permet de récupérer 
le premier élément d'une paire, snd permet de récupérer le second élément *)

let inv(a,b) =
  (snd(a,b),fst(a,b));;

let invPatternMatching couple = match couple with
  |[] -> []
  |[a;b] -> [b;a]
  |_ -> failwith "L'élément reçu n'est pas un couple !";;

let rec fib n =
  if n = 0 then 0
  else if n = 1 then 1
  else (fib (n-1)) + (fib (n-2));; 

(* autre version *)

let rec fib2 n = match n with
  |0 -> 0
  |1 -> 1
  |_ -> (fib2(n-1)) + (fib2(n-2));;

let rec pgcd a b =
  if a > b then (pgcd b a)
  else if a = 0 then b
  else (pgcd (b mod a) a);; 

let rec pair n =
  if n = 0 then true else(impair(n-1))
and impair n = 
  if n = 0 then false else (pair(n-1));;

(* appeler une fonction fact2 n m à l'intérieur d'une fonction fact n *)

let factTailRecur n = let rec factTailRecur' n total = 
                        if n > 0 then (factTailRecur' (n-1) (total * n))
                        else total
  in factTailRecur' n 1;; 

let fibTailRecur n = let rec fibTailRecur' n a b = 
                       if n = 0 then a
                       else if n = 1 then b
                       else (fibTailRecur' (n-1) b (a+b))
  in fibTailRecur' n 0 1;;

let rec exp x n = 
  if n = 0 then 1
  else x * (exp x (n-1));;

let exp' x n = let rec exp'2 x n total =
                 if n > 0 then (exp'2 x (n-1) (total*x))
                 else total
  in exp'2 x n 1;;

let expCalc x n = let rec expCalc' x n total count =
                    if n > 0 then (expCalc' x (n-1) (total * x) (count +1))
                    else [total, count]
  in expCalc' x n x 0;;


let rec normalSum n m = 
  if n = m then n
  else n + (normalSum (n+1) m);; 

let sum1 n m = let rec sum1' n m total =
                 if n < m then (sum1' (n+1) m (total + (normalSum n m)))
                 else if n = m then total + n
                 else failwith "Erreur : m inferieur a n !"
  in sum1' n m 0;;