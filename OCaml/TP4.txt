(* Exercice 1 *)

type exp = Binaire of string * exp * exp | Unaire of string * exp | Value of int;; 

let var = Binaire( "+", Value(5), Unaire("-",Value(1)));;
  
let rec eval e = match e with
  |Value(a) -> a
  |Unaire("-", sub) -> - (eval sub)
  |Unaire("+", sub) -> (eval sub) 
  |Binaire("+", sub1, sub2) -> (eval sub1) + (eval sub2)
  |Binaire("-", sub1, sub2) -> (eval sub1) - (eval sub2)
  |Binaire("*", sub1, sub2) -> (eval sub1) * (eval sub2)
  |Binaire("/", sub1, sub2) -> (eval sub1) / (eval sub2)
  |_ -> failwith "Syntax error";; 
  
eval (Binaire("-", (Binaire("+", Value(1), Value(2))), Value(3)));;

(* Exercice 2 *)

(* Utilisation d'un type de liste spécifique 

type listFunctions = Couple of string * string * listFunctions | End of string * string;; 

let rec search_function_by_operation str list = match list with 
  |Couple(str1, str2, endOfList) -> if str1 = str then str2 else (search_function_by_operation str endOfList)
  |End(str1, str2) -> if str1 = str then str2 else failwith "L'opération n'est associée à aucune fonction";;

let list_of_functions = (Couple("+","addition",(End("-","soustraction"))));; *)

(* Utilisation d'une liste normale *)

let add a b = a + b;; 
let minus a b = a - b;;

let list_funs = [("+", add); ("-", minus)];;

let rec search_fun_by_op op list = match list with
  |[] -> failwith "L'opération n'est associée à aucune valeur !"
  |(a,b)::r when a = op -> b
  |a::b -> search_fun_by_op op b;;
