from models import Bank
import unittest


class Tests_exo_3(unittest.TestCase):

    def test_bank_creation(self):
        bank = Bank()
        assert bank.id == 0
        assert not bank.account_list
