from models import Account, Bank
from Exo2 import Tests_exo_2


def create_account(bank, client):
    return Account(bank, client)


def create_bank():
    return Bank()


if __name__ == '__main__':
    tests = Tests_exo_2()
    tests.test_account_creation()
    # Pas de cas limite pour l'ajout
    tests.test_add()
    # Cas limite: le découvert bancaire n'est pas géré (< 0)
    tests.test_remove()
    tests.test_add_accounts()
