bank_id = 0
account_id = 0


class Overdraft(Exception):
    pass


class Client:

    def __init__(self, name):
        self.name = name


class Account:

    def __init__(self, bank, client):
        global account_id
        self.id = account_id
        self.client = client
        self.amount = 0
        self.bank = bank
        bank.account_list.append(self)
        account_id += 1

    def __add__(self, other):
        return self.amount + other.amount

    def __iadd__(self, other):
        self.update_amount(other.amount)
        return self

    def __isub__(self, other):
        if other.amount > 0:
            self.update_amount(-other.amount)
        else:
            self.update_amount(other.amount)
        return self

    def update_amount(self, amount):
        if self.amount + amount < 0:
            raise Overdraft("Découvert non géré")
        else:
            self.amount += amount


class Bank:

    def __init__(self):
        global bank_id
        self.id = bank_id
        self.account_list = list()
        bank_id += 1
