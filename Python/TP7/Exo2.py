from models import Account, Bank, Client, Overdraft
import unittest


class Tests_exo_2(unittest.TestCase):

    def test_account_creation(self):
        client = Client("Delouvy")
        bank = Bank()
        account = Account(bank, client)
        assert account.amount == 0
        assert account.client == client
        assert bank.account_list.__contains__(account)
        assert account.id == bank.id == 0

    def test_add(self):
        client = Client("Delouvy")
        bank = Bank()
        account = Account(bank, client)
        account.update_amount(100)
        assert account.amount == 100

    def test_remove(self):
        client = Client("Delouvy")
        bank = Bank()
        account = Account(bank, client)
        self.assertRaises(Overdraft, account.update_amount, -10)

    def test_add_accounts(self):
        client = Client("Delouvy")
        bank = Bank()
        account1 = Account(bank, client)
        account1.update_amount(100)
        account2 = Account(bank, client)
        account2.update_amount(50)
        assert (account1 + account2 == 150)
