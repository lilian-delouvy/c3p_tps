class Affect:
    def __init__(self, name, expr):
        self.name = name
        self.expr = expr

    def __repr__(self):
        return f"{self.name} = {self.expr}"

    def evaluate(self, context_ops, context_vars):
        context_vars[self.name] = self.expr.evaluate(context_ops, context_vars)


class Expr:
    def evaluate(self, context_ops, context_vars):
        ...


class Var(Expr):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name

    def evaluate(self, context_ops, context_vars):
        return context_vars[self.name]


class Value(Expr):
    def __init__(self, val):
        self.val = val

    def __repr__(self):
        return str(self.val)

    def evaluate(self, context_ops, context_vars):
        return self.val


class Binop(Expr):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right

    def __repr__(self):
        return f"{self.left} {self.op} {self.right}"

    def evaluate(self, context_ops, context_vars):
        eval_left = self.left.evaluate(context_ops, context_vars)
        eval_right = self.right.evaluate(context_ops, context_vars)
        function = context_ops[self.op]
        return function(eval_left, eval_right)


class Unop(Expr):
    def __init__(self, op, expr):
        self.op = op
        self.expr = expr

    def __repr__(self):
        return f"({self.op} {self.expr})"

    def evaluate(self, context_ops, context_vars):
        eval_expr = self.expr.evaluate(context_ops, context_vars)
        function = context_ops[self.op]
        return function(0, eval_expr)


class If(Expr):
    def __init__(self, cond, then, else_):
        self.cond = cond
        self.then = then
        self.else_ = else_

    def __repr__(self):
        return f"if ({self.cond}) ? {self.then} : {self.else_}"

    def evaluate(self, context_ops, context_vars):
        cond = self.cond.evaluate(context_ops, context_vars)
        if cond == 0:
            return self.then.evaluate(context_ops, context_vars)
        return self.else_.evaluate(context_ops, context_vars)


def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def mult(x, y):
    return x * y


def div(x, y):
    return x // y


def mod(x, y):
    return x % y


context_operateurs = {
    "+": add,
    "-": sub,
    "*": mult,
    "/": div,
    "%": mod,
}

if __name__ == '__main__':
    # # 1+3 + (-4)
    expr1 = Binop("*",
                  left=Value(8),
                  right=Binop("/",
                              left=Value(3),
                              right=Unop("-", Value(6))))

    # print(expr1.evaluate(context_operateurs))
    #
    # # 1 + 3
    expr2 = Binop("-", left=Value(1), right=Value(3))
    # print(expr2.evaluate(context_operateurs))

    inst1 = Affect("a", expr1)
    # print(inst1)
    context_var = {}
    inst1.evaluate(context_operateurs, context_var)
    print(context_var)

    expr3 = Binop("+", Var("a"), Value(5))
    print(expr3.evaluate(context_operateurs, context_var))

    inst2 = Affect('b', If(Value(0), Value(4), Value(9)))
    print(inst2)

    context_var = {}
    inst2.evaluate(context_operateurs, context_var)
    print(context_var)
