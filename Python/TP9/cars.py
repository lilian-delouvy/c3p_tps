""""Spec de voiture cars.py"""

from spec import specification, Attribute
import spec


@specification
class Engine(object):
    ...


@specification
class Vehicle(object):
    number_of_wheels = Attribute(int)
    engine = Attribute(Engine)


@specification
class Car(Vehicle):
    ...


if __name__ == '__main__':
    header = spec.gen_class_header(Vehicle)
    print(Vehicle.number_of_wheels.type)
