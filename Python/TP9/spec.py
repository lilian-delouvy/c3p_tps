""""Fonctions relatives au marquage de classes et à la génération spec.py"""
from cars import Engine


def specification(cls):
    cls.specification = True
    return cls


class Attribute(object):
    def __init__(self, t):
        self.type = t


def gen_class_header(cls):
    header = """class {}({}):""".format(cls.__name__, gen_heritage(cls))
    return header


def gen_heritage(cls):
    string = ""
    for child in cls.__bases__:
        string = string + child.__name__ + ", "
    return string[0:len(string) - 1]


def gen_class_body(cls):
    ...


def __init__(self, number_of_wheels=None, engine=None):
    self.number_of_wheels = number_of_wheels
    self.engine = engine

    @property
    def number_of_wheels(self):
        return self.number_of_wheels

    @number_of_wheels.setter
    def number_of_wheels(self, value):
        assert value is None or isinstance(value, int)
        self.number_of_wheels = value

    @engine.setter
    def engine(self, value):
        assert value is None or isinstance(value, Engine)
        self.engine = value


def gen_class(cls):
    ...


def collect_specifications():
    ...
