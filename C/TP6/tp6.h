//
// Created by lilia on 09/10/2020.
//

#ifndef TP6_TP6_H
#define TP6_TP6_H

typedef struct{
    int id;
    int credit;
    char *owner;
}Compte;

void createAccount(Compte *newAccount, int credit, char *newOwner);

void freeAccount(Compte *account);

void addToCredit(Compte *account, int value);

void withdrawToCredit(Compte *account, int value);

Compte getAccountById(int id);

Compte * getAccountsByUser(char * user);

void deleteAllAccounts();

#endif //TP6_TP6_H
