#include <stdio.h>

#define CTEST_MAIN
#define CTEST_COLOR_OK

#include "ctest.h"
#include "tp6.h"

//Exercice 2

Compte* tabComptes[50];
int nbComptes = 0;

void createAccount(Compte *newAccount, int credit, char *newOwner){
    newAccount->id = nbComptes;
    newAccount->credit = credit;
    newAccount->owner = (char*)malloc(sizeof(newOwner));
    strcpy(newAccount->owner,newOwner);
    tabComptes[nbComptes] = newAccount;
    nbComptes++;
}

void freeAccount(Compte *account){
    free(account->owner);
    account->credit = NULL;
    account->id = NULL;
    free(account);
}

//Exercice 3

CTEST(suite3, test1){
    typedef struct{
        int id;
        int credit;
        char * owner;
    }testAccount;

    testAccount *newAccount;
    ASSERT_NOT_NULL(newAccount);
}

CTEST(suite3, test2){
    Compte newAccount;
    createAccount(&newAccount, 1000, "Lilian");
    ASSERT_EQUAL(1000,newAccount.credit);
    newAccount.credit += 500;
    ASSERT_EQUAL(1500, newAccount.credit);
}

void addToCredit(Compte *account, int value){
    account->credit += value;
}

CTEST(suite3, test3){
    Compte newAccount;
    createAccount(&newAccount, 1000, "Lilian");
    newAccount.credit -= 500;
    ASSERT_EQUAL(500, newAccount.credit);
}

void withdrawToCredit(Compte *account, int value){
    account->credit -= value;
}

CTEST(suite3, test4){
    nbComptes = 0;
    int searchedAccount = 1;
    Compte newAccount1;
    Compte newAccount2;
    Compte newAccount3;
    createAccount(&newAccount1, 10, "Lilian");
    createAccount(&newAccount2, 110, "Lilian");
    createAccount(&newAccount3, 1000, "Lilian");
    ASSERT_NOT_NULL(tabComptes[searchedAccount]->owner);
//    freeAccount(tabComptes[searchedAccount]);
//    ASSERT_NULL(tabComptes[searchedAccount]->owner);
}

Compte getAccountById(int id){
    return *tabComptes[id];
}

CTEST(suite3, test5){
    nbComptes = 0;
    int nbSameOwnerAccounts = 0;
    Compte *tabResults;
    Compte newAccount1;
    Compte newAccount2;
    Compte newAccount3;
    createAccount(&newAccount1, 10, "Lilian");
    createAccount(&newAccount2, 110, "Vincent");
    createAccount(&newAccount3, 1000, "Lilian");
    for(int i = 0; i < 3; i++){
        if(strcmp(tabComptes[i]->owner, "Lilian") == 0){
            nbSameOwnerAccounts++;
        }
    }
    ASSERT_EQUAL(2, nbSameOwnerAccounts);
}

Compte * getAccountsByUser(char * user){
    int nbUserAccounts = 0;
    Compte* accounts = (Compte*)malloc(sizeof(Compte));
    for(int i = 0; i<50; i++){
        if(tabComptes[i] != NULL){
            if(strcmp(tabComptes[i],user) == 0){
                accounts[0] = *tabComptes[i];
                nbUserAccounts++;
            }
        }
    }
    return accounts;
}

void deleteAllAccounts(){
    for(int i = 0; i < 50; i++){
        freeAccount(&tabComptes[i]);
        free(&tabComptes[i]);
    }
    free(tabComptes);
}

int main(int argc, const char *argv[])
{
    for(int i = 0; i < 50; i++){
        tabComptes[i] = (Compte *)malloc(sizeof(Compte));
    }

    int result = ctest_main(argc, argv);

//    Compte newAccount;
//    createAccount(&newAccount, 1000, "Lilian");
//    printf("%d %d %d", newAccount.id, tabComptes[nbComptes-1]->credit, nbComptes);
//    freeAccount(&tabComptes[0]);

    deleteAllAccounts();

    return result;
}
