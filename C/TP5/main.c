#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    unsigned char memory[4096];
    unsigned char tab[16];
    unsigned short tabMemory;
    unsigned short PC;
    int nbOctets;
}chip8;

chip8* create_machine(){
    return malloc(sizeof(chip8));
}

void init(chip8 * element){
    for(int i = 0; i<4096;i++){
        if(i<16){
            element->tab[i] = 0;
        }
        element->memory[i] = 0;
    }
    element->tabMemory = 0;
    element->PC = 0;
}

//Exercice 2 : chargement d'une ROM

void read_ROM(chip8* element, char* path){
    FILE *file = fopen(path, "r");
    int countOctets;
    if(file == NULL){
        printf("Erreur dans la lecture du fichier !");
        exit(0);
    }
    else{
        element->nbOctets = countOctets = fread(element->memory + 0x200, 1, 4096, file);
        element->PC = 0x200;
    }
    fclose(file);
}

//Exercice 3 : decodage d'une instruction

char* concatChar(char* str1, char* str2){
    char * result = (char*) malloc(sizeof(1 + strlen(str1) + strlen(str2)));
    strcpy(result, str1);
    strcat(result, str2);
    return result;
}

int main() {
    chip8* element = create_machine();
    init(element);
    char* str1 = "test";
    char* str2 = "test2";
    printf("%s", concatChar(str1,str2));
    printf("\nHello, World!\n");
    return 0;
}
